/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.thidarat.oxoop;

/**
 *
 * @author acer
 */
public class Board {

    private char table[][] = {{'-', '-', '-'}, {'-', '-', '-'}, {'-', '-', '-'}};
    private Player currenPlayer;
    private Player O;
    private Player X;
    private int count;
    private int row;
    private int col;
    public boolean win = false;
    public boolean draw = false;

    public Board(Player O, Player X) {
        this.O = O;
        this.X = X;
        this.currenPlayer = O;
        this.count = 0;
    }

    public void switchPalyer() {
        if (currenPlayer == O) {

            currenPlayer = X;

        } else {
            currenPlayer = O;
        }
    }

    public boolean setRowCol(int row, int col) {
        if(isWin()||isDraw())return  false;

        this.row = row;
        this.col = col;

        if (row > 3 || row > 3 || row < 1 || col < 1) {
            return false;
        }
        if (table[row - 1][col - 1] != '-') {
            return false;
        }
        table[row - 1][col - 1] = currenPlayer.getSymbol();

        if (checkWin()) {
            updateStat();
            this.win = true;
            return true;
        }
        if (checkDraw()) {
            
            O.Draw();
            X.Draw();
            
            this.draw = true;
            return true;
        }
        count++;
        switchPalyer();
        return true;
    }

    private void updateStat() {
        if (this.currenPlayer == O) {
            O.Win();
            X.Loss();
        } else {
            X.Win();
            O.Loss();
        }
    }

    public boolean isWin() {
        return win;
    }

    public boolean isDraw() {
        return draw;
    }

    public boolean checkDraw() {
        if (count == 8) {
            return true;
        }
        return false;
    }

    public char[][] getTable() {
        return table;
    }

    public Player getCurrenPlayer() {
        return currenPlayer;
    }

    public Player getO() {
        return O;
    }

    public Player getX() {
        return X;
    }

    public int getCount() {
        return count;
    }

    public boolean checkWin() {

        if (checkVetical()) { //ตั้ง

            return true;

        } else if (checkHrizontal()) {//นอน
            return true;

        } else if (checkX()) { //ทเเยง
            return true;
        }
        return false;
    }

    public boolean checkVetical() {
        for (int i = 0; i < table.length; i++) {
            if (table[i][col - 1] != currenPlayer.getSymbol()) {
                return false;
            }
        }
        return true;
    }

    public boolean checkHrizontal() {
        for (int i = 0; i < table.length; i++) {
            if (table[row - 1][i] != currenPlayer.getSymbol()) {
                return false;
            }
        }
        return true;
    }

    public boolean checkX() {
        if (checkX1()) {
            return true;
        } else if (checkX2()) {
            return true;
        }
        return false;
    }

    public boolean checkX1() { //11 22 33
        for (int i = 0; i < table.length; i++) {
            if (table[i][i] != currenPlayer.getSymbol()) {
                return false;
            }
        }
        return true;
    }

    public boolean checkX2() { // 13 22 31
        for (int i = 0; i < table.length; i++) {
            if (table[i][2 - i] != currenPlayer.getSymbol()) {
                return false;
            }
        }
        return true;
    }

}
