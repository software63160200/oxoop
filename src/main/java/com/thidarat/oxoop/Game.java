/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.thidarat.oxoop;

import java.util.Scanner;

/**
 *
 * @author acer
 */
public class Game {

    private Player O;
    private Player X;
    private int row;
    private int col;
    private Board board;
    Scanner kb = new Scanner(System.in);

    public Game() {
        this.O = new Player('O');
        this.X = new Player('X');

    }

    public void newBoard() {
        this.board = new Board(O, X);
    }

    public void showWelcome() {
        System.out.println("Welcome to OX Game");
    }

    public void showTable() {
        char[][] table = this.board.getTable();
        for (int i = 0; i < table.length; i++) {
            for (int j = 0; j < table[i].length; j++) {
                System.out.print(table[i][j]);
            }
            System.out.println();
        }
    }

    public void showTurn() {
        Player player = board.getCurrenPlayer();
        System.out.println("Turn " + player.getSymbol());
    }

    public void inputRowCol() {
        while (true) {
            System.out.println("Please input row, col: ");
            row = kb.nextInt();
            col = kb.nextInt();
            if (board.setRowCol(row, col)) {
                return;
            }
        }
    }
    
    public boolean isFinish(){
        if(board.isDraw()||board.isWin()){
            return true;
        }
        return  false;
    }

    
  
    
    public void showResult(){
        if(board.isDraw()){
            System.out.println("Draw!!!");
        }else if(board.win){
            System.out.println(board.getCurrenPlayer().getSymbol()+"Win!!!");
        }
    }
    
    public void showStat(){
        System.out.println(O);
        System.out.println(X);
    }

}
